/*==========================================================
- Autor:           Dev. Ernildo Porfirio
- Empresa:         Kryptus
- Módulo:          Node
- Data de Criação: 13/Fev/2021
==========================================================*/

/* INCLUSÃO DE BIBLIOTECAS */
#include <stdio.h>
#include <stdlib.h>
#include "List.h"
#include "../Node/Node.h"

/* DEFINIÇÃO DA ENTRUTURA */
struct list {
  Node* root;
};

/* FUNÇÃO QUE VERIFICA SE A LISTA É VÁLIDA (MÉTODO PRIVADO) */
unsigned char isValid(List* l) {
  unsigned char result = 0x00;
  
  if (!l) 
    printf("Lista inválida!\n");
  else
    result = 0x01;

  return result; 
}

/* FUNÇÃO QUE CRIA UMA LISTA VAZIA */
List* createList() {
  List* l = (List*)malloc(sizeof(List));

  if (!l) return l;

  l->root = NULL;
  
  return l;
}

/* FUNÇÃO ADICIONA UM VALOR NA LISTA */
void put(List* l, int value)
{
  if (!l)
    printf("Lista inválida");
  else
  {
    Node* newNode = createNode(value);
    l->root = appendNodeInLast(l->root, newNode);
  }
}

/* FUNÇÃO QUE EXIBE O VALOR ESPECIFICADO */
void get(List* l, int value) {
  if (isValid(l)) {
    Node* temp = getPosNode(l->root, value);
    if (temp)
      printf("%d\n", getValueNode(temp));
  }
}

/* FUNÇÃO QUE EXIBE TODA A LISTA */
void list(List *l) {
  if (isValid(l)) {
    Node* temp = l->root;
    
    if (temp) {
      for (temp; temp != NULL; temp = getNextNode(temp))
        printf("%d ", getValueNode(temp));
      printf("\n");
    }
    else printf("Lista vazia!\n");
  }
}

/* FUNÇÃO QUE REMOVE UM ELEMENTO ESPECÍFICO DA LISTA */
void remov(List *l, int value) {
  if (isValid(l)) {
    l->root = removeNode(l->root, value);
  }
}

/* FUNÇÃO QUE LIMPA TODA A LISTA */
void clear(List *l) {
  if (isValid(l)) {
    l->root = removeAllNodes(l->root);
  }
}

/* FUNÇÃO QUE DELETA A LISTA */
void deleteList(List *l) {
  if (isValid(l)) {
    l->root = removeAllNodes(l->root);
    free(l);
  }
}
