#ifndef LIST_H
#define LIST_H

typedef struct list List;

List *createList();
void put(List *l, int value);
void get(List *l, int value);
void list(List *l);
void remov(List *l, int value);
void clear(List *l);
void deleteList(List *l);

#endif