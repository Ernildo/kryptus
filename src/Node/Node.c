/*==========================================================
- Autor:           Dev. Ernildo Porfirio
- Empresa:         Kryptus
- Módulo:          Node
- Data de Criação: 13/Fev/2021
==========================================================*/

/* INCLUSÃO DE BIBLIOTECAS */ 
#include <stdio.h>
#include <stdlib.h>
#include "Node.h"

/* DEFINIÇÃO DA ESTRUTURA DE DADOS */
struct node {
  int val;
  Node *next;
};

/* FUNÇÃO DE CRIAÇÃO DE UM NÓ */
Node* createNode(int value) {
  Node* nd = (Node*)malloc(sizeof(Node));

  if (!nd) return nd;

  nd->val = value;
  nd->next = NULL;

  return nd;
}

/* FUNÇÃO QUE INSERE ELEMENTO NO FINAL DA LISTA */
Node* appendNodeInLast(Node* r, Node* n) {
  if (!r) return n;

  Node* temp = r;
  for (temp; temp->next != NULL; temp = temp->next);
  temp->next = n;

  return r;
}

/* FUNÇÃO QUE PEGA O ENDEREÇO DO NÓ QUE CONTÉM O VALOR ESPECIFICADO */
Node* getPosNode(Node* r, int value){
  Node* temp = r;

  while (temp) {
    if (temp->val == value) return temp;

    temp = temp->next;
  }

  return temp;
}

/* FUNÇÃO QUE RETORNA O VALOR DO NÓ ESPECIFICADO */
int getValueNode(Node* r) {
  return r->val;
}

/* FUNÇÃO QUE PEGA O ENDEREÇO DO IRMÃO DO NÓ ESPECIFICADO */
Node* getNextNode(Node* r) {
  return r->next;
}

/* FUNÇÃO QUE REMOVE O NÓ QUE POSSUI O VALOR ESPECIFICADO */
Node* removeNode(Node* r, int value) {
  Node* temp = getPosNode(r, value);
  
  if (temp) {

    // eleiminar o primeiro da fila
    if (temp == r){
      Node* rmNode = temp;
      temp = temp->next;
      free(rmNode);
      return temp;
    }

    Node* t = r;
    for (t; t->next != temp; t = getNextNode(t));
    Node* rmNode = t->next;
    t->next = rmNode->next;
    free(rmNode);
  }

  return r; 
}

/* FUNÇÃO QUE REMOVE TODOS OS NÓS DA LISTA */
Node* removeAllNodes(Node* r){
  Node* temp = r;
  
  while (temp) {
    Node* rmNode = temp;
    temp = getNextNode(temp);
    free(rmNode);
  }

  return temp; 
}