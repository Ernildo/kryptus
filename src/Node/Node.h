#ifndef NODE_H
#define NODE_H

typedef struct node Node;

Node* createNode(int value);
Node* appendNodeInLast(Node *r, Node *n);
Node* getPosNode(Node* r, int value);
int   getValueNode(Node* r);
Node* getNextNode(Node* r);
Node* removeNode(Node* r, int value);
Node* removeAllNodes(Node* r);

#endif