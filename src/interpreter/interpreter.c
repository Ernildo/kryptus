/*==========================================================
- Autor:           Dev. Ernildo Porfirio
- Empresa:         Kryptus
- Módulo:          Interpreter
- Data de Criação: 13/Fev/2021
==========================================================*/


#include <string.h>
#include "interpreter.h"

int exitComand(char* i) {
  return strcmp(i, "exit");
}

unsigned char getComand(char* comand) {
  unsigned char comandNumber = NOP;
  
  if (!strcmp(comand, "get"))
    comandNumber = GET;
  else if (!strcmp(comand, "put"))
    comandNumber = PUT;
  else if (!strcmp(comand, "list"))
    comandNumber = LIST;
  else if (!strcmp(comand, "clear"))
    comandNumber = CLEAR;
  else if (!strcmp(comand, "remove"))
    comandNumber = REMOVE;
  else comandNumber = NOP;

  return comandNumber;
}