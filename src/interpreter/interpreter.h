#ifndef INTERPRETER_H
#define INTERPRETER_H

#define NOP     0x10
#define GET     0x01
#define PUT     0x02
#define LIST    0x03
#define CLEAR   0x04
#define REMOVE  0x05

int exitComand(char* i);
unsigned char getComand(char* comand);

#endif