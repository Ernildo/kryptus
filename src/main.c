/*==========================================================
- Autor:           Dev. Ernildo Porfirio
- Empresa:         Kryptus
- Módulo:          Main
- Data de Criação: 13/Fev/2021
==========================================================*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "List/List.h"
#include "interpreter/interpreter.h"

int main(int argc, char **argv) {
  
  List* myList = createList();

  system("clear");
  char imput[80];
  int loop = 1;

  do {  
    printf("prompt > ");
    scanf(" %[^\n]s ", imput);
    
    char comand[10];
    int value;

    if (loop = exitComand(imput)) {
        
      sscanf(imput, "%s %d", comand, &value);
      
      switch (getComand(comand)) {
        case GET: 
          get(myList, value); 
        break;
        case PUT: 
          put(myList, value); 
        break;
        case LIST: 
          list(myList); 
        break;
        case REMOVE: 
          remov(myList, value); 
        break;
        case CLEAR: 
          clear(myList); 
        break;
        case NOP: 
          printf("comando não compreendido\n"); 
        break;
      }    
    } 
    else deleteList(myList);
    
  } while(loop);

  return 0;
}